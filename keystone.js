// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'Fitness Pug',
	'brand': 'Fitness Overflow',

	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'pug',

	'emails': 'templates/emails',

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
});

// Load your project's Models
keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes
keystone.set('routes', require('./routes'));

// Mailgun email provider
keystone.set('email transport', 'mailgun');
keystone.set('mailgun api key', 'key-206fd8a3cd8caa2d1443e73b3c2fcaba');
keystone.set('mailgun domain', 'mail.fitnessoverflow.com');

// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.
keystone.set('email locals', {
	logo_src: '/images/logo-email.gif',
	logo_width: 194,
	logo_height: 76,
	theme: {
		email_bg: '#f9f9f9',
		link_color: '#2697de',
		buttons: {
			color: '#fff',
			background_color: '#2697de',
			border_color: '#1a7cb7',
		},
	},
});

// Load your project's email test routes
keystone.set('email tests', require('./routes/emails'));

// secure images
keystone.set('cloudinary secure', true);

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
	posts: ['posts', 'post-categories'],
	enquiries: 'enquiries',
	users: 'users',
});

keystone.set('siteName', 'Fitness Overflow');
keystone.set('baseUrl', 'https://fitnessoverflow.com/');


if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
    console.log('----------------------------------------'
        + '\nWARNING: MISSING MAILGUN CREDENTIALS'
        + '\n----------------------------------------'
        + '\nYou have opted into email sending but have not provided'
        + '\nmailgun credentials. Attempts to send will fail.'
        + '\n\nCreate a mailgun account and add the credentials to the .env file to'
        + '\nset up your mailgun integration');
}


// Start Keystone to connect to your database and initialise the web server

keystone.start();
