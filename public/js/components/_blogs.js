/**
 * Created by sharjeel on 13/03/17.
 */
$(document).ready(function () {


    if ( $('#disqus_thread').length ) {

        //var disqus_developer = 1;
        var PAGE_URL = $('#post-title').data('slug');
        var PAGE_IDENTIFIER = $('#post-title').data('postid');

        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

         var disqus_config = function () {
         this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
         };


        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://fitnessoverflow.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();

    }

    $('.facebook-share').on('click', function (e) {
        e.preventDefault();

        var url = $(this).data('posturl');

        FB.ui({
            method: 'share',
            display: 'popup',
            href: url,
        }, function(response){
            console.log(response);
        });

    });



    // $('.twitter-share').on('click', function (e) {
    //     e.preventDefault();
    //     window.open( $(this).attr('href'), 'Share on Twitter', 'width=300,height=300' );
    // });

    /*$('#ajax-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            url: '/newsletter/signup',
            method: 'post',
            dataType: 'json',
            data: $('#ajax-form').serialize(),
            success: function (response) {
                if (response.error) {
                    $('.signup__aside .form__errors').html('<p>Please provide a valid Email address.</p>');
                }
                else if (response.success) {
                    $('.signup__aside .form__errors').html('');
                    var html = '<div class="form__success">' +
                        '<p>Thank you for signing up. We have sent you a request for email confirmation.</p>' +
                        '</div>';
                    $('.signup__aside .form__container').html(html);
                }
                else {
                    $('.signup__aside .form__errors').html('<p>Something went wrong. Please try again.</p>');
                }
            },
            error: function (err) {
                console.log(err);
                $('.signup__aside .form__errors').html('<p>Something went wrong. Please try again.</p>');
            }
        });

    });*/

});