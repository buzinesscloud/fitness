var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Subscriber Model
 * ==========
 */
var Subscriber = new keystone.List('Subscriber', {
    nocreate: true,
    noedit: true,
});

Subscriber.add({
	name: { type: Types.Name },
	email: { type: Types.Email, required: true, unique:true, index: true },
	token: { type: String, required: true, index:true },
}, 'Status', {
	isVerified: { type: Boolean, label: 'Email address is verified', default: false },
    isActive: { type: Boolean, label: 'Subscribed to Newsletter', default: true },
    isAddedToList: { type: Boolean, label: 'Added to Newsletter List', default: false },
});


// Email is verified
Subscriber.schema.virtual('isEmailVerified').get(function () {
    return this.isVerified;
});

// Subscribed to Newsletter
Subscriber.schema.virtual('isSubscribed').get(function () {
    return this.isActive;
});

// Added to Newsletter List in Mailgun
Subscriber.schema.virtual('isInList').get(function () {
    return this.isAddedToList;
});


Subscriber.schema.pre('save', function (next) {
    this.wasNew = this.isNew;
    next();
});

Subscriber.schema.post('save', function () {
    if (this.wasNew || (this.isActive == true && this.isVerified == false)) {
        this.sendVerificationEmail();
    }
});


Subscriber.schema.methods.sendVerificationEmail = function (callback) {
    if (typeof callback !== 'function') {
        callback = function (err) {
            if (err) {
                console.error('There was an error sending the verification email:', err);
            }
        };
    }

    if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
        console.log('Unable to send email - no mailgun credentials provided');
        return callback(new Error('could not find mailgun credentials'));
    }

    var subscriber = this;
    var brand = keystone.get('brand');
    var SITE_URL = keystone.get('baseUrl');

    new keystone.Email({
        templateName: 'subscriber-verification',
        transport: 'mailgun',
    }).send({
        to: subscriber.email,
        from: {
            name: 'Fitness Overflow',
            email: 'noreply@mail.fitnessoverflow.com',
        },
        subject: 'Response Required: Please confirm your email address',
        subscriber: subscriber,
        brand: brand,
        SITE_URL: SITE_URL,
    }, callback);


};


/**
 * Registration
 */
Subscriber.defaultSort = '-createdAt';
Subscriber.defaultColumns = 'name, email, isVerified, isActive, isAddedToList';
Subscriber.register();
