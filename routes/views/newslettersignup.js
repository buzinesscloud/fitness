/**
 * Created by sharjeel on 23/04/17.
 */
var keystone = require('keystone');
var async = require('async');

var Subscriber = keystone.list('Subscriber');

exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'subscriber';
    locals.formData = req.body || {};
    locals.validationErrors = '';
    locals.subscriberSubmitted = false;


    // On POST requests, add the Subscriber item to the database
    view.on('post', { action: 'emailsignup' }, function (next) {

        async.series([
            function (callback) {

                if (!req.body.email) {
                    locals.validationErrors = 'Please provide a valid email address.';
                    // pass true to return from the async series
                    return callback(true);
                }
                // proceed to next async function
                return callback();

            }, function (callback) {

                Subscriber.model.findOne({ email: req.body.email }, function(err, subscriber) {

                    if (err) {
                        return callback(true);
                    }

                    if (subscriber) {
                        if (subscriber.isVerified && subscriber.isActive && subscriber.isAddedToList) {
                            locals.validationErrors = 'Subscriber already exists with that email address.';
                        }
                        else {
                            subscriber.isActive = true;
                            subscriber.save();
                            locals.subscriberSubmitted = true;
                        }
                        return callback(true);
                    }

                    return callback();

                });

            }, function (callback) {

                var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                var token = '';
                for (var i = 48; i > 0; --i) {
                    token += chars[Math.round(Math.random() * (chars.length - 1))];
                }
                var subscriberData = {
                    name: req.body.name ? req.body.name.full : '',
                    email: req.body.email,
                    token: token
                };

                var SubscriberModel = Subscriber.model;
                var newSubscriber = new SubscriberModel(subscriberData);
                newSubscriber.save(function (err) {
                    if (!err) {
                        locals.subscriberSubmitted = true;
                    }
                    return callback(err);
                });

            }
        ], function (err) {
            next();
        });

    });


    /*view.on('post', { action: 'emailsignup' }, function (next) {

        var newSubscriber = new Subscriber.model();
        var updater = newSubscriber.getUpdateHandler(req);

        var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var token = '';
        for (var i = 48; i > 0; --i) {
            token += chars[Math.round(Math.random() * (chars.length - 1))];
        }

        var subscriberObject = req.body;
        subscriberObject.token = token;


        updater.process(req.body, {
            flashErrors: false,
            fields: 'name, email, token',
            errorMessage: 'There was a problem registering your email',
        }, function (err) {
            if (err) {
                locals.validationErrors = err.error;
                console.log(err)
            } else {
                locals.subscriberSubmitted = true;
            }
            if ( req.xhr == true ) {
                // Send JSON Response to AJAX Request
                res.send({
                    'error': locals.validationErrors,
                    'success': locals.subscriberSubmitted
                });
            }
            else {
                next();
            }
        });
    });*/

    view.render('newsletter-signup');
};
