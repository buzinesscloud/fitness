/**
 * Created by sharjeel on 25/04/17.
 */
var keystone = require('keystone');
var MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
var MAILGUN_DOMAIN = process.env.MAILGUN_DOMAIN;
var LIST_ADDRESS = 'fitnessnoreply@mail.fitnessoverflow.com';
var mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: MAILGUN_DOMAIN});


exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'newsletter';
    locals.filters = {
        token: req.params.token,
    };
    locals.unSubscribed = false;
    locals.alreadyUnsubscribed = false;

    var Subscriber = keystone.list('Subscriber');
    var subscriberObject = {};

    Subscriber.model.findOne({
        token: locals.filters.token,
    }).exec()
        .then(function (subscriber) {

            subscriberObject = subscriber;
            if ( subscriber.isVerified == false && subscriber.isAddedToList == false && subscriber.isActive == false ) {
                locals.alreadyUnsubscribed = true;
                return;
            }
            else {

                // set email verification flag to false
                subscriber.isVerified = false;

                var list = mailgun.lists(LIST_ADDRESS);

                return list.members(subscriber.email).update({subscribed: false});

            }

        }).then(function (response) {

            if (response != undefined && response.member && response.member.subscribed == false && subscriberObject.email) {
                subscriberObject.isAddedToList = false;
                subscriberObject.isActive = false;
                subscriberObject.save();
                locals.unSubscribed = true;
            }

            // Render the view
            view.render('newsletter-unsubscribe');

    }).catch(function (err) {
        console.log(err);
    });

};