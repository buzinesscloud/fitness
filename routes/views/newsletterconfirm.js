/**
 * Created by sharjeel on 23/04/17.
 */
var keystone = require('keystone');
var async = require('async');

// Mailgun config
var MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
var MAILGUN_DOMAIN = process.env.MAILGUN_DOMAIN;
var LIST_ADDRESS = 'fitnessnoreply@mail.fitnessoverflow.com';
var mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: MAILGUN_DOMAIN});

exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'newsletter';
    locals.filters = {
        token: req.params.token,
    };
    locals.subscriberConfirmed = false;
    locals.alreadyConfirmed = false;

    var Subscriber = keystone.list('Subscriber');

    view.on('init', function (next) {
        async.series([
            function (callback) {

                Subscriber.model.findOne({ token: locals.filters.token }, function(err, subscriber) {

                    if (err || !subscriber) {
                        return callback(true);
                    }

                    if ( subscriber.isVerified == true && subscriber.isAddedToList == true ) {
                        locals.alreadyConfirmed = true;
                        return callback(true);
                    }

                    var member = [
                        {
                            subscribed: true,
                            address: subscriber.email,
                            name: subscriber.name.first + ' ' + subscriber.name.last
                        }
                    ];

                    var list = mailgun.lists(LIST_ADDRESS);

                    list.members().add({
                        members: member,
                        upsert: true
                    }, function (err, data) {
                        if (err) {
                            return callback(true);
                        }
                        subscriber.isVerified = true;
                        subscriber.isAddedToList = true;
                        subscriber.save(function (err) {
                            if (!err) {
                                locals.subscriberConfirmed = true;
                            }
                            return callback();
                        });
                    });

                });

            }
        ], function (err) {
            next();
        });
    });

    /*Subscriber.model.findOne({
        token: locals.filters.token,
    }).exec()
        .then(function (subscriber) {


            if ( subscriber.isVerified == true && subscriber.isAddedToList == true ) {
                locals.alreadyConfirmed = true;
                // Render the view
                view.render('newsletter-confirm');
            }
            else {

                // set email verification flag to true
                subscriber.isVerified = true;
                subscriber.save();

                if (subscriber.isAddedToList == false) {

                    var member = [
                        {
                        subscribed: true,
                        address: subscriber.email,
                        name: subscriber.name.first + ' ' + subscriber.name.last
                        }
                    ];

                    var list = mailgun.lists(LIST_ADDRESS);

                    list.members().add({
                        members: member,
                        upsert: true
                    }).then(function (data) {
                        console.log(data);
                        subscriber.isAddedToList = true;
                        subscriber.save();
                        locals.subscriberConfirmed = true;
                        // Render the view
                        view.render('newsletter-confirm');

                    }, function (err) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                    });

                }
                else {
                    locals.subscriberConfirmed = true;

                    // Render the view
                    view.render('newsletter-confirm');
                }

            }

        }, function (err) {
            if (err) {
                console.log(err);
                return;
            }
        });*/

    // Render the view
    view.render('newsletter-confirm');

};