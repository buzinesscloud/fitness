var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'blog';
	locals.filters = {
		post: req.params.post,
	};
	locals.data = {
		posts: [],
		topPosts: [],
	};

    var Post = keystone.list('Post');

    Post.model.findOne({
		state: 'published',
		slug: locals.filters.post,
	}).populate('author categories')
		.exec()
		.then(function (post) {

			// view counter
            post.views += 1;
            post.save();

            locals.data.post = post;

            locals.siteMeta.title = post.meta.title || keystone.get('siteName');
            locals.siteMeta.description = post.meta.description;
            locals.siteMeta.image = post.image.url;
            locals.siteMeta.type = 'article';
            locals.siteMeta.url = keystone.get('baseUrl') + 'blog/post/' + post.slug;

            if (post.categories && post.categories.length) {
            	return Post.model.find({ _id: { $ne: post._id } }).where('categories').in([post.categories[0]._id]).limit(5).exec();
            }
            else {
            	return '';
			}
        }).then(function (data) {

		locals.data.posts = data;

        return Post.model.find().where('state', 'published').sort('-views').limit(5).exec();

    }).then(function (topPosts) {

    	locals.data.topPosts = topPosts;

        // Render the view
        view.render('post');

    }).catch(function (err) {
		console.log(err)
    });




};
