/**
 * Created by sharjeel on 27/03/17.
 */

var https = require('https');

exports.count = function(req, res) {

    var appID = '619333364923271';
    var appSecret = 'd0835b179609eeef6a71db93cc8299b4';

    // Do not need to request access token
    // var optionsAccess = {
    //     host: 'graph.facebook.com',
    //     port: '443',
    //     path: '/oauth/access_token?client_id='+appID+'&client_secret='+appSecret+'&grant_type=client_credentials'
    // };


    var countCallback = function (accessToken) {

        // Do not need to request access token
        //var accessTokenJSON = JSON.parse(accessToken);
        //var tokenValue = accessTokenJSON['access_token'];


        var optionsCount = {
            host: 'graph.facebook.com',
            port: '443',
            method: 'GET',
            path: '/?id=http:\/\/'+encodeURIComponent(req.params.url)+'&access_token='+accessToken
        };

        https.get(optionsCount, function (response) {
            var data = '';
            response.on('data', function(d) {
                data += d;
            });

            response.on('end', function() {
                res.send( JSON.parse(data) );
            });

        }).on('error', function(err) {
            res.send({'error':err});
        });
    };

    // Do not need to request access token
    // var accessTokenCallback = function (response) {
    //
    //     var body = '';
    //     response.on('data', function(d) {
    //         body += d;
    //     });
    //
    //     response.on('end', function() {
    //         countCallback(body);
    //     });
    //
    // };
    //
    // https.get(optionsAccess, accessTokenCallback).on('error', function(err) {
    //     console.log(err)
    // });

    // pass app ID and app secret as access token
    countCallback(appID+'|'+appSecret);

};